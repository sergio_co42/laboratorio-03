package com.academiasmoviles.sesion02

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_destino.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Click Boton Enviar
        btnEnviar.setOnClickListener {
            val nombres = edtNombres.text.toString()
            val edad = edtEdad.text.toString()
            val textParvovirus= cBParvovirus.text.toString()
            val textHepatitis= cBHepatitis.text.toString()
            val textRabia= cBRabia.text.toString()
            val textLeucemia= cBLeucemia.text.toString()
            val textMixomatosis= cBMixomatosis.text.toString()
            val textNull= "No seleccionada"
            /*val perro= ivMascota.setImageResource(R.drawable.perro).toString()
            val gato= ivMascota.setImageResource(R.drawable.gato).toString()
            val conejo= ivMascota.setImageResource(R.drawable.conejo).toString()*/


            //Validar campos nombres
            if (nombres.isEmpty()){
                mostrarMensaje("Debe ingresar sus nombres")
                return@setOnClickListener
            }
            //Validad campos edad
            if (edad.isEmpty()){
                mostrarMensaje("Debe ingresar su edad")
                return@setOnClickListener
            }

        var mascota = if (rbPerro.isChecked) {
            "Perro"

        }else if (rbGato.isChecked) {
            "Gato"

        }else {
            "Conejo"

        }
        val bundle= Bundle()
            bundle.apply {
                putString("key_nombres", nombres)
                putString("key_edad", edad)
                putString("key_mascota", mascota)
                //Validaciones de RButton
                /*when {
                    rbPerro.isChecked -> {
                        putInt("key_perro", R.drawable.perro)
                    }
                    rbGato.isChecked -> {
                        putInt("key_gato", R.drawable.gato)
                    }
                    rbConejo.isChecked -> {
                        putInt("key_conejo", R.drawable.conejo)
                    }
                }*/

                //Validaciones de checkbox
                if (cBParvovirus.isChecked) {
                    putString("key_parvovirus", textParvovirus)
                } else{
                    putString("key_parvovirus", textNull)
                }
                if (cBHepatitis.isChecked) {
                    putString("key_hepatitis", textHepatitis)
                } else{
                    putString("key_hepatitis", textNull)
                }
                if (cBRabia.isChecked) {
                    putString("key_rabia", textRabia)
                } else{
                    putString("key_rabia", textNull)
                }
                if (cBLeucemia.isChecked) {
                    putString("key_leucemia", textLeucemia)
                } else {
                    putString("key_leucemia", textNull)
                }
                if (cBMixomatosis.isChecked) {
                    putString("key_mixomatosis", textMixomatosis)
                } else {
                    putString("key_mixomatosis", textNull)
                }
            }



            val intent = Intent(this, DestinoActivity::class.java).apply {
                putExtras(bundle)
                when {
                    rbPerro.isChecked -> {
                        putExtra("key_perro", R.drawable.perro)
                    }
                    rbGato.isChecked -> {
                        putExtra("key_gato", R.drawable.gato)
                    }
                    rbConejo.isChecked -> {
                        putExtra("key_conejo", R.drawable.conejo)
                    }
                }
             }
            startActivity(intent)

       }

        }
    fun mostrarMensaje(mensaje: String){
        Toast.makeText(this, "$mensaje", Toast.LENGTH_SHORT).show()
    }

}