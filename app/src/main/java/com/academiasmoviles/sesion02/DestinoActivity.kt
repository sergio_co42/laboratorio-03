package com.academiasmoviles.sesion02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_destino.*
import kotlinx.android.synthetic.main.activity_main.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)
        //Obtenemos el bundle
        val bundleRecepcion : Bundle? = intent.extras


        //let
        bundleRecepcion?.let {  bundleLibreDeNull ->
            val nombres =  bundleLibreDeNull.getString("key_nombres","Desconocido")
            val edad = bundleLibreDeNull.getString("key_edad","0")
            val genero = bundleLibreDeNull.getString("key_mascota","")
            val v1 = bundleLibreDeNull.getString("key_parvovirus","")
            val v2 = bundleLibreDeNull.getString("key_hepatitis","")
            val v3 = bundleLibreDeNull.getString("key_rabia","")
            val v4 = bundleLibreDeNull.getString("key_leucemia","")
            val v5 = bundleLibreDeNull.getString("key_mixomatosis","")
            val perro = intent.getIntExtra("key_perro",0)
            val gato = intent.getIntExtra("key_gato",0)
            val conejo = intent.getIntExtra("key_conejo",0)



            tvNombresDestino.text = "Nombres: $nombres"
            tvEdadDestino.text = "Edad: $edad"
            tvGeneroDestino.text = "Mascota: $genero"
            tv1.text = "Vacuna: $v1"
            tv2.text = "Vacuna: $v2"
            tv3.text = "Vacuna: $v3"
            tv4.text = "Vacuna: $v4"
            tv5.text = "Vacuna: $v5"

            ivMascota.setImageResource(perro)
            ivMascota.setImageResource(gato)
            ivMascota.setImageResource(conejo)


        }


    }
}